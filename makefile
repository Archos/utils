
build: obj/utils.o
	ar rcs libutils.a obj/utils.o

install: build /usr/include/nathan /usr/lib/nathan
	cp libutils.a /usr/lib/nathan
	cp src/utils.h /usr/include/nathan

/usr/include/nathan:
	mkdir /usr/include/nathan

/usr/lib/nathan:
	mkdir /usr/lib/nathan

uninstall:
	rm -f /usr/lib/nathan/libutils.a /usr/include/nathan/utils.h

obj/utils.o: src/utils.c obj
	cc -o $@ -c $<

obj:
	mkdir obj

clean:
	rm -f libutils.a
