#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"

int
strnext(const char *str, const char *delim)
{
    int ret = strcspn(str, delim);
    str += ret;
    ret += strspn(str, delim);
    return ret;
}

void
strparse(const char *str, struct pcond *cond)
{
    while (cond->func) {
        if (*str == cond->key)
            cond->func(str, cond->data);
        else
            ++cond;
    }
}

void
strparseall(const char *str, const char *delim, struct pcond *conds)
{
    while (*str) {
		strparse(str, conds);
		str += strnext(str, delim);
	}
}

void
strextractf(const char *str, float *ptr)
{
	*ptr = atof(str);
}

void
strextracti(const char *str, int *ptr)
{
	*ptr = atoi(str);
}
