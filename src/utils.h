
#define LEN(x)                (sizeof(x) / sizeof(*(x))
#define DUPLICATE(x, quan)    memcpy(malloc(SIZE(x, quan) *x, SIZE(x, quan))
#define REALLOC(x, type, num) x = (type*)realloc(x, sizeof(type), num)
#define CALLOC(type, quan)    (type*)calloc(sizeof(type), quan)

typedef void (spfunc)(const char *, void *);

struct pcond {
    char key;
	spfunc *func;
    void *data;
};

int
strnext(const char *str, const char *delim);

void
strparse(const char *str, struct pcond *cond);

void
strparseall(const char *str, const char *delim, struct pcond *conds);

void
strextractf(const char *str, float *ptr);

void
strextracti(const char *str, int *ptr);

